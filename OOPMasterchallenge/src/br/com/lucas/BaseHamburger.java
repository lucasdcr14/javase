package br.com.lucas;

public class BaseHamburger {

    private String name;
    private String breadRollType;
    private String meat;
    private double price;

    private String additinal1Name;
    private double additional1Price;

    private String additinal2Name;
    private double additional2Price;

    private String additinal3Name;
    private double additional3Price;

    private String additinal4Name;
    private double additional4Price;

    public BaseHamburger(String name, String breadRollType, String meat, double price) {
        this.name = name;
        this.breadRollType = breadRollType;
        this.meat = meat;
        this.price = price;
    }

    public void addHamgurgerAddition1(String name, double price) {
        this.additinal1Name = name;
        this.additional1Price = price;
    }

    public void addHamgurgerAddition2(String name, double price) {
        this.additinal2Name = name;
        this.additional2Price = price;
    }

    public void addHamgurgerAddition3(String name, double price) {
        this.additinal3Name = name;
        this.additional3Price = price;
    }

    public void addHamgurgerAddition4(String name, double price) {
        this.additinal4Name = name;
        this.additional4Price = price;
    }

    public double itemizeHamburger() {
        double hamburgerPrice = this.price;
        System.out.println(this.name + " hamburger" + " on a " + this.breadRollType + " roll" +
                " with " + this.meat +
                " price is " + this.price);
        if(this.additinal1Name != null) {
            hamburgerPrice += this.additional1Price;
            System.out.println("Added " + this.additinal1Name + " for extra " + this.additional1Price);
        }
        if(this.additinal2Name != null) {
            hamburgerPrice += this.additional2Price;
            System.out.println("Added " + this.additinal2Name + " for extra " + this.additional2Price);
        }
        if(this.additinal3Name != null) {
            hamburgerPrice += this.additional3Price;
            System.out.println("Added " + this.additinal3Name + " for extra " + this.additional3Price);
        }
        if(this.additinal1Name != null) {
            hamburgerPrice += this.additional4Price;
            System.out.println("Added " + this.additinal4Name + " for extra " + this.additional4Price);
        }

        return hamburgerPrice;
    }

}

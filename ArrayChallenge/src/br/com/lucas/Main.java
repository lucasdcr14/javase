package br.com.lucas;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        // Create a program using arrays that sorts a list of integers in descending order.
        // Descending order is highest value to lowest.
        // In other words if the array had the values in it 106, 26, 81, 5, 15 your program should
        // ultimately have an array with 106,81,26, 15, 5 in it.
        // Set up the program so that the numbers to sort are read in from the keyboard.
        // Implement the following methods - getIntegers, printArray, and sortIntegers
        // getIntegers returns an array of entered integers from keyboard
        // printArray prints out the contents of the array
        // and sortIntegers should sort the array and return a new array containing the sorted numbers
        // you will have to figure out how to copy the array elements from the passed array into a new
        // array and sort them and return the new sorted array.

        int[] array = getIntegers(5);

        printArray(array);

        int[] arraySorted = sortIntegers(array);

        System.out.println(" ");

        printArray(arraySorted);

    }

    private static void printArray(int[] array) {

        for(int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

//    private static int[] sortIntegers(int[] array) {
//
//        int[] arraySorted = new int[array.length];
//        int indexSorted = 0;
//
//        for(int i = 0; i < array.length; i++) {
//            int big = array[i];
//            int index = i;
//
//            for(int j = 0; j < array.length; j++) {
//                if(big <= array[j]) {
//                    big = array[j];
//                    index = j;
//                }
//            }
//
//            arraySorted[indexSorted] = big;
//            array[index] = 0;
//            indexSorted ++;
//        }
//
//        return arraySorted;
//    }

    private static int[] sortIntegers(int[] array) {

//        int[] sortedArray = new int[array.length];
//        for(int i = 0; i < array.length; i++) {
//            sortedArray[i] = array[i];
//        }

        int[] sortedArray = Arrays.copyOf(array, array.length);

        boolean flag = true;
        int temp;
        while (flag) {
            flag = false;
            for(int i = 0; i < sortedArray.length - 1; i++) {
                if(sortedArray[i] < sortedArray[i + 1]) {
                    temp = sortedArray[i];
                    sortedArray[i] =sortedArray[i + 1];
                    sortedArray[i+1] = temp;
                    flag = true;
                }
            }
        }

        return sortedArray;
    }

    private static int[] getIntegers(int number) {

        int[] array = new int[number];
        for(int i = 0; i < number; i++) {
            array[i] = scanner.nextInt();
        }

        return array;
    }
}

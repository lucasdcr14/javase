package br.com.lucas;

public class Main {

    public static void main(String[] args) {

        boolean gameOver = true;
        int score = 500;
        int levelCompleted = 5;
        int bonus = 100;

        if(score == 5000) {
            System.out.println("Your score was 5000.");
            System.out.println("This was executed");
        }

        if(score == 5000)
            System.out.println("Your score was 5000.");
        System.out.println("This was executed");

        if(score < 5000) {
            System.out.println("Your score was less than 5000.");
        } else {
            System.out.println("Got here");
        }

        if(score <= 5000) {
            System.out.println("Your score was less than 5000.");
        } else {
            System.out.println("Got here");
        }

        /*if(score < 5000 && score > 1000) {
            System.out.println("Your score was less than 5000 but greater than 1000.");
        } else if (score < 1000) {
            System.out.println("Your score was less than 1000");
        } else {
            System.out.println("Got here");
        }*/

        if(gameOver) { // if(gameOver == true) ...
            int finalScore = score + (levelCompleted * bonus);
            finalScore += 1000;
            System.out.println("Your final score was " + finalScore);
        }

        //int savedFinalScore = finalScore;

        // Print out a second score on the screen with the folowing
        // score set to 1000
        // levelCompleted set to 8
        // bonus set to 20
        // But make sure the first printout above still displays as well

        score = 1000;
        levelCompleted = 8;
        bonus = 20;

        if(gameOver) { // if(gameOver == true) ...
            int finalScore = score + (levelCompleted * bonus);
            System.out.println("Your final score was " + finalScore);
        }
    }
}

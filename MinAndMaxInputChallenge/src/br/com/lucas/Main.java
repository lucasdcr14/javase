package br.com.lucas;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int max = 0;
        int min = 0;

        System.out.println("Enter number: ");
        boolean isAnInt = scanner.hasNextInt();
        if(isAnInt) {
            int number = scanner.nextInt();
            scanner.nextLine();
            max = number;
            min = number;
        }


        while(isAnInt) {
            System.out.println("Enter number: ");
            isAnInt = scanner.hasNextInt();

            if(isAnInt) {
                int number = scanner.nextInt();
                scanner.nextLine();

                if (number > max)
                    max = number;

                if (number < min)
                    min = number;
            } else {
                scanner.close();
                break;
            }

            scanner.nextLine();
        }

        System.out.println("Max number: " + max);
        System.out.println("Min number: " + min);

    }
}

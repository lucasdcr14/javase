package br.com.lucas;

import java.io.IOException;
import java.util.Scanner;

public class uri1001 {

    public static void main(String[] args) throws IOException {

        int a, b, total;

        Scanner scanner = new Scanner(System.in);
        a = scanner.nextInt();
        b = scanner.nextInt();

        total = a + b;

        System.out.println("X = " + total);

        scanner.close();
    }
}

package br.com.lucas;

public class OperadoresRelacionaisLogicos {

	public static void main(String[] args) {
		
		// (==) igual
		boolean resultado; // true ou false	
		resultado = true;
		resultado = 1 == 1;
		int n1 = 1;
		int n2 = 2;
		resultado = n1 == n2;
		System.out.println(resultado);
		
		// != Diferente
		n1 = 1;
		n2 = 2;
		resultado = n1 != n2;
		System.out.println(resultado);
		
		// > Maior
		n1 = 1;
		n2 = 2;
		resultado = n1 > n2;
		System.out.println(resultado);
		
		// < Menor
		n1 = 1;
		n2 = 2;
		resultado = n1 < n2;
		System.out.println(resultado);
		
		// >= Maior or igual
		n1 = 1;
		n2 = 2;
		resultado = n1 >= n2;
		System.out.println(resultado);
		
		// <= Menor ou igual
		n1 = 1;
		n2 = 2;
		resultado = n1 <= n2;
		System.out.println(resultado);
		
		// Operadores lógicos
		// && e
		// || ou
		resultado = true && true;
		System.out.println(resultado);
		
		resultado = true || false;
		System.out.println(resultado);
		
	}

}

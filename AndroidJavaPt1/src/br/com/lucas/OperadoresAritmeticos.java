package br.com.lucas;

public class OperadoresAritmeticos {

	public static void main(String[] args) {
		// Somar (+)
		// Subtrair (-)
		// Multiplicar (*)
		// Dividir (/)
		// Módulo - resto da divisão (%)
		
		int numero1 = 10;
		int numero2 = 5;
		int resultado = 0;
		
		resultado = numero1 + numero2;
		System.out.println(resultado);
		
		resultado = numero1 - numero2;
		System.out.println(resultado);
		
		resultado = numero1 * numero2;
		System.out.println(resultado);
		
		resultado = numero1 / numero2;
		System.out.println(resultado);
		
		resultado = numero1 % numero2;
		System.out.println(resultado);
	}
}

package br.com.lucas;

public class Loops {

    /*

        (condição) {
            Execute até que a condição seja satisfeita
        }

     */

    public static void main(String[] args) {


        int numero = 5;
        while(numero < 5) {
            System.out.println("Executou " + numero);
            numero++;
        }

        String[] postagens = {"Bom dia amigos", "postagem 2", "postagem 3"};
        numero = 0;
        while(numero < postagens.length) {
            System.out.println("Executou " + postagens[numero]);
            numero++;
        }

        numero = 0;
        do {
            System.out.println("Executou " + postagens[numero]);
            numero++;
        } while (numero < 5);

        for (int n = 0; n < postagens.length; n++) {
            System.out.println("Executou " + postagens[numero]);
        }

    }
}

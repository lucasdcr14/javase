package br.com.lucas;

public class Exercise21 {

    public static void main(String[] args) {
        printFactors(6);
        System.out.println("");
        printFactors(32);
        System.out.println("");
        printFactors(10);
        System.out.println("");
        printFactors(-1);
        System.out.println("");
        System.out.println(1);
    }

    public static void printFactors(int number) {
        if(number >= 1) {
            for(int i = 1; i <= number / 2; i++) {
                if(number % i == 0)
                    System.out.println(i);
            }
            System.out.println(number);
        } else {
            System.out.println("Invalid Value");
        }
    }
}

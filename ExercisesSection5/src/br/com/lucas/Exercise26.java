package br.com.lucas;

public class Exercise26 {

    public static void main(String[] args) {
        System.out.println(getLargestPrime(21));
        System.out.println(getLargestPrime(217));
        System.out.println(getLargestPrime(0));
        System.out.println(getLargestPrime(45));
        System.out.println(getLargestPrime(-1));
        System.out.println(getLargestPrime(7));
    }

    public static int getLargestPrime(int number) {
        if(number > 1) {
            int largestPrime = 1;

            for (int i = 1; i <= number; i++) {
                if(number % i == 0) {
                    boolean isPrime = true;
                    for(int j = 2; j <= i / 2; j++) {
                        if(i % j == 0)
                            isPrime = false;
                    }

                    if(isPrime)
                        largestPrime = i;
                }
            }

            return largestPrime;
        } else {
            return -1;
        }
    }
}

package br.com.lucas;

public class Exercise24 {

    public static void main(String[] args) {
        System.out.println(canPack(1, 0, 4));
        System.out.println(canPack(1, 0, 5));
        System.out.println(canPack(1, 5, 4));
        System.out.println(canPack(2, 2, 11));
        System.out.println(canPack(-3, 2, 12));
        System.out.println("");
        System.out.println(canPack(2, 1, 5));
    }

    public static boolean canPack(int bigCount, int smallCount, int goal) {
        if(bigCount >= 0 && smallCount >= 0 && goal >= 0) {
            int total = bigCount * 5 + smallCount;

            if(total == goal)
                return true;

            for (int j = 0; j <= bigCount; j++) {
                if(5 * j == goal) {
                    return true;
                }

                for(int i = 0; i <= smallCount; i++ ) {
                    if(1 * i == goal)
                        return true;
                    if(j * 5 + 1 * i == goal)
                        return true;
                }
            }

            return false;
        } else {
            return false;
        }
    }
}

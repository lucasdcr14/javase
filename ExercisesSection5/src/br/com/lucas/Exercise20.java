package br.com.lucas;

public class Exercise20 {
    public static void main(String[] args) {
        System.out.println(getGreatestCommonDivisor(12, 30));
    }

    public static int getGreatestCommonDivisor(int first, int second) {
        if(first >= 10 && second >= 10) {
            int gcm = 0;

            int biggest = 0;
            if(first > second)
                biggest = first;
            else
                biggest = second;

            for(int i = 1; i < biggest; i++) {
                if(first % i == 0 && second % i == 0)
                    gcm = i;
            }

            return gcm;
        } else {
            return -1;
        }
    }
}

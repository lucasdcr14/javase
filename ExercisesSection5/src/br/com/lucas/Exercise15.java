package br.com.lucas;

public class Exercise15 {

    public static void main(String[] args) {

        System.out.println(isPalindrome(-1221));
        System.out.println(isPalindrome(707));
        System.out.println(isPalindrome(11212));
    }

    public static boolean isPalindrome(int number) {
        if(number < 0)
            number = Math.abs(number);
        int numberReceive = number;
        int reverse = 0;

        while (number > 0) {
            reverse *= 10;

            int digit = number % 10;
            reverse += digit;

            number /= 10;
        }

        return numberReceive == reverse;
    }
}


package br.com.lucas;

public class Exercise23 {

    public static void main(String[] args) {
//        numberToWords(123);
//        System.out.println("");
//        numberToWords(1010);
//        System.out.println("");
//        numberToWords(1000);
//        System.out.println("");
//        numberToWords(-12);
//        System.out.println(reverse(-2));
//        System.out.println(getDigitCount(0));
        numberToWords(0);
    }

    public static void numberToWords(int number) {
        if(number >= 0) {

            int digitsNumber = getDigitCount(number);
            number = reverse(number);
            int digits = 0;
            while(number > 0) {
                digits++;
                int digit = number % 10;

                switch (digit) {
                    case 0:
                        System.out.println("Zero");
                        break;
                    case 1:
                        System.out.println("One");
                        break;
                    case 2:
                        System.out.println("Two");
                        break;
                    case 3:
                        System.out.println("Three");
                        break;
                    case 4:
                        System.out.println("Four");
                        break;
                    case 5:
                        System.out.println("Five");
                        break;
                    case 6:
                        System.out.println("Six");
                        break;
                    case 7:
                        System.out.println("Seven");
                        break;
                    case 8:
                        System.out.println("Eight");
                        break;
                    case 9:
                        System.out.println("Nine");
                        break;

                }

                number /= 10;
            }

            if(digitsNumber != digits) {
                for(int i = 1; i <= digitsNumber - digits; i++) {
                    System.out.println("Zero");
                }
            }
        } else {
            System.out.println("Invalid Value");
        }
    }

    public static int reverse(int number) {

        int numberABS = Math.abs(number);
        int reverse = 0;
        while(numberABS > 0) {
           reverse *= 10;
           int digit = numberABS % 10;

           reverse += digit;

            numberABS /= 10;
        }

        if(number < 0)
            return reverse * -1;
        else
            return reverse;
    }

    public static int getDigitCount(int number) {
        if (number >= 0) {
            int count = 0;
            do {
                int digit = number % 10;

                count++;

                number /= 10;
            } while(number > 0);

            return count;
        } else {
            return -1;
        }
    }
}

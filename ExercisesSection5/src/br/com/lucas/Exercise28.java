package br.com.lucas;

import java.util.Scanner;

public class Exercise28 {

    public static void main(String[] args) {
        inputThenPrintSumAndAverage();
    }

    public static void inputThenPrintSumAndAverage() {

        Scanner scanner = new Scanner(System.in);

        int count = 0;
        int sum = 0;
        int number = 0;
        long average = 0;

        boolean isAnInt = scanner.hasNextInt();
        if(isAnInt) {
            number = scanner.nextInt();
            scanner.nextLine();
            count++;
            sum += number;
        }

        while (isAnInt) {
            isAnInt = scanner.hasNextInt();
            if(isAnInt) {
                number = scanner.nextInt();
                scanner.nextLine();
                count++;
                sum += number;
            }
        }

        if(sum != 0)
            average = Math.round((double) sum / count);

        System.out.println("SUM = " + sum + " AVG = " + average);
    }
}

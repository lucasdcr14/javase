package br.com.lucas;

public class Exercise22 {

    public static void main(String[] args) {
        System.out.println(isPerfectNumber(6));
        System.out.println(isPerfectNumber(28));
        System.out.println(isPerfectNumber(5));
        System.out.println(isPerfectNumber(-1));
    }

    public static boolean isPerfectNumber(int number) {
        if(number > 0) {
            int sumDivisors = 0;

            for(int i = 1; i <= number / 2; i++) {
                if(number % i == 0)
                    sumDivisors += i;
            }

            return sumDivisors == number;
        } else {
            return false;
        }
    }
}

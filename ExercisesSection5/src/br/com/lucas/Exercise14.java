package br.com.lucas;

public class Exercise14 {

    public static void main(String[] args) {
        System.out.println(sumOdd(1, 100));
        System.out.println(sumOdd(-1, 100));
        System.out.println(sumOdd(100, 100));
        System.out.println(sumOdd(100, -100));
        System.out.println(sumOdd(100, 1000));
    }

    public static boolean isOdd(int number) {
        if(number > 0) {
            if(number % 2 != 0)
                return true;
            else
                return false;

        } else {
            return false;
        }
    }

    public static int sumOdd(int start, int end) {
        if(start > 0 && end >= start && end > 0) {
            int sum = 0;
            for(int i = start; i <= end; i++) {
                if(isOdd(i))
                    sum += i;
            }
            return sum;
        } else {
            return -1;
        }
    }
}

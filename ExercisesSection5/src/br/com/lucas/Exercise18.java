package br.com.lucas;

public class Exercise18 {

    public static void main(String[] args) {
//        System.out.println(hasSharedDigit(12, 23));
//        System.out.println(hasSharedDigit(9, 99));
//        System.out.println(hasSharedDigit(15, 15));
//        System.out.println(hasSharedDigit(12, 34));
        System.out.println(hasSharedDigit(12, 13));
    }

    public static boolean hasSharedDigit(int a, int b) {
        int numberA = a;
        int numberB = b;
        if(a >= 10 && a <= 99 && b >= 10 && b <= 99) {
            while (numberA > 0) {

                int digitA = numberA % 10;

                numberB = b;
                while (numberB > 0) {

                    int digitB = numberB % 10;

                    if(digitA == digitB)
                        return true;

                    numberB /= 10;
                }

                numberA /= 10;
            }

            return false;
        } else {
            return false;
        }
    }
}

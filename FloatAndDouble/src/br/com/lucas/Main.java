package br.com.lucas;

public class Main {

    public static void main(String[] args) {
        // width of int = 32 (4 bytes);
	    //int myIntValue = 5;
        int myIntValue = 5 / 2;

        // width of float = 32 (4 bytes);
        //float myFloatValue = 5f;
        //float myFloatValue = (float) 5.4;
        //float myFloatValue = 5f / 2f;
        float myFloatValue = 5f / 3f;

        // width of double = 64 (8 bytes);
	    //double myDoubleValue = 5d;
        //double myDoubleValue = 5d / 2d;
        double myDoubleValue = 5d / 3d;

        System.out.println("myIntvalue = " + myIntValue);
        System.out.println("myFloatValue = " + myFloatValue);
        System.out.println("myDoubleValue = " + myDoubleValue);

        // Convert a given number of pounds to kilograms
        // 1. Create a variable to store the number of pounds
        // 2. Calculate the number of kilograms for the number above and store in a variable
        // 3. Print Out the result
        //
        // NOTES: 1 pound is equal to 0.45359237 kilograms.

        double pounds = 200;
        double kilograms = pounds * 0.45359237;

        System.out.println("pounds = " + pounds);
        System.out.println("kilograms = " + kilograms);
        // 90.7185
        double pi = 3.141_592_7d;
    }
}

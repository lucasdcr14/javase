package br.com.lucas;

public class BankAccount {

    private String number;
    private double balance;
    private String nameCustomer;
    private String emailCustomer;
    private String phoneNumberCustomer;

    public BankAccount() {
        this("56789", 2.50, "Default name", "Default address", "Default phone");
        System.out.println("Empty constructor called");
    }

    public BankAccount(String number, double balance, String nameCustomer, String emailCustomer, String phoneNumberCustomer) {
        System.out.println("Account constructor with parameters called");
        this.number = number;
        this.balance = balance;
        this.nameCustomer = nameCustomer;
        this.emailCustomer = emailCustomer;
        this.phoneNumberCustomer = phoneNumberCustomer;
    }

    public BankAccount(String nameCustomer, String emailCustomer, String phoneNumberCustomer) {
        this("99999", 100.55, nameCustomer, emailCustomer, phoneNumberCustomer);
//        this.nameCustomer = nameCustomer;
//        this.emailCustomer = emailCustomer;
//        this.phoneNumberCustomer = phoneNumberCustomer;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getEmailCustomer() {
        return emailCustomer;
    }

    public void setEmailCustomer(String emailCustomer) {
        this.emailCustomer = emailCustomer;
    }

    public String getPhoneNumberCustomer() {
        return phoneNumberCustomer;
    }

    public void setPhoneNumberCustomer(String phoneNumberCustomer) {
        this.phoneNumberCustomer = phoneNumberCustomer;
    }

    public void deposit(double amount) {
        balance += amount;
        System.out.println("Deposit of " + amount + " made. New balance is " + balance);
    }

    public void withdrawal(double amount) {
        if(balance - amount > 0) {
            balance -= amount;
            System.out.println("Withdrawal of " + amount + " processed. Remaining balance = " + balance);
        } else {
            System.out.println("Only " + balance + " available. Withdrawal not processed.");
        }

    }
}

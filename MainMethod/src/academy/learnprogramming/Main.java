package academy.learnprogramming;

/**
 *
 */
public class Main {

    /**
     * This is main method
     *
     * @param args commnad line arguments
     */
    public static void main(String[] args) {
        // printing size of arguments
        //
        //
        System.out.println("args-size " + args.length);

        /*
        * printing the arguments
        * another line
        * */
        for(int i = 0; i < args.length; i++) {
            System.out.println("args[" + i + "] = " + args[i]);
        }

    }

    /**
     * Calculates sum of two integers
     *
     * @param a operand
     * @param b operand
     * @return
     */
    public static int sum(int a, int b) {
        return a + b;
    }
}

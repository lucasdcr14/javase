package br.com.lucas;

public class Main {

    public static void main(String[] args) {
        int[] array = {1, 3, 6, 4, 1, 2};
        System.out.println(solution(array));
    }

    public static int solution(int[] A) {
        int smallest = A[0];

        for(int i = 1; i < A.length; i++) {
            if(A[i] >= 0 && A[i] < smallest)
                smallest = A[i];
        }
    }
}

package br.com.lucas;

public class Main {

    public static void main(String[] args) {

        // int has a width of 32
        int myIntvalue = 1000;
        int myMinValue = -2_147_483_648;
        int myMaxValue = 2_147_483_647;
        int myTotal = (myMinValue/2);
        System.out.println("myTotal = " + myTotal);

        // byte has a width of 8
        byte myByteValue = 10;
        byte myMinByteValue = -128;
        byte myMaxByteValue = 127;
        byte myNewByteValue = (byte) (myMinByteValue/2);
        System.out.println("mynewByteValue = " + myNewByteValue);

        // short has a width of 16
        short myShortValue = 30000;
        short myMinShortvalue = -32768;
        short myMaxShortvalue = 32767;
        short myNewShortValue = (short) (myMinShortvalue/2);
        System.out.println("myNewShortValue = " + myNewShortValue);

        // long has a width of 64
        long myLongValue = 100;
        long myMinLongValue = -9_223_372_036_854_775_808L;
        long myMaxLongValue = 9_223_372_036_854_775_807L;

        //1. Create a byte variable and set it to any valid byte number.
        //2. Create a short variable and set it to any valid short number.
        //3. Create a int variable and set it to any valid int number.
        //4. Create a variable of type long, and make it equal to 50000 + 10 times the sum of the byte, plus the short plus the int

        byte myByte = 10;
        short myShort = 20;
        int myInt = 50;

        long total = 50000L + 10L * (myByte + myShort + myInt);
        short shortTotal = (short) (1000 + 10 * (myByte + myShort + myInt));
        System.out.println("Exercise = " + total);
        System.out.println("shortTotal = " + shortTotal);

    }
}

package br.com.lucas;

public class Exercise3 {

    public static void main(String[] args) {
        System.out.println(shouldWakeUp(true, 1));
        System.out.println(shouldWakeUp(false, 2));
        System.out.println(shouldWakeUp(true, 8));
        System.out.println(shouldWakeUp(true, -1));
    }

    public static boolean shouldWakeUp(boolean barking, int hourOfDay) {
        if(barking) {
            if(hourOfDay >= 0 && hourOfDay <= 23) {
                if(hourOfDay > 22 || hourOfDay < 8)
                    return true;
                else
                    return false;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

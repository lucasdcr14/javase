package br.com.lucas;

public class Exercise1 {

    public static void main(String[] args) {

        toMilesPerHour(1.5);
        toMilesPerHour(10.25);
        toMilesPerHour(-5.6);
        toMilesPerHour(25.42);
        toMilesPerHour(75.114);

        printConversion(1.5);
        printConversion(10.25);
        printConversion(-5.6);
        printConversion(25.42);
        printConversion(75.114);

    }

    public static long toMilesPerHour(double kilometersPerHour) {
        if(kilometersPerHour < 0)
            return -1;
        else
            return Math.round(kilometersPerHour / 1.609);
    }

    public static void printConversion(double kilometersPerhour) {
        if(kilometersPerhour < 0)
            System.out.println("Invalid Value");
        else
            System.out.println(kilometersPerhour + " km/h = " + toMilesPerHour(kilometersPerhour) + " mi/h");
    }
}

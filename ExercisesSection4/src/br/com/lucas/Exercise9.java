package br.com.lucas;

public class Exercise9 {

    public static void main(String[] args) {
        printYearsAndDays(525600);
        printYearsAndDays(1051200);
        printYearsAndDays(561600);

    }

    public static void printYearsAndDays(long minutes) {
        if (minutes >= 0) {
            int year = (int) minutes / 60 / 24 / 365;
            int days = (int) minutes / 60 / 24 % 365;

            System.out.println(minutes + " min = " + year + " y and " + days + " d");
        } else {
            System.out.println("Invalid Value");
        }
    }
}

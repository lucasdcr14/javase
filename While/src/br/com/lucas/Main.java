package br.com.lucas;

public class Main {

    public static void main(String[] args) {
//	    int count = 0;
//	    while (count != 5) {
//            System.out.println("Count value is " + count);
//            count++;
//        }
//
//	    /*for(int i = 1; i < 7; i++) {
//            System.out.println("Count value is " + i);
//        }*/
//
//	    count = 1;
//	    while(true) {
//	        if(count == 5)
//	            break;
//
//            System.out.println("Count value is " + count);
//            count++;
//        }
//
//	    count = 1;
//	    do {
//            System.out.println("Count value was " + count);
//            count++;
//
//            if(count < 100)
//                break;
//        } while (count != 6);

        int number = 4;
        int finishedNumber =  20;

        while (number <= finishedNumber) {
            number++;
            if(!isEvenNumber(number)) {
                continue;
            }
            System.out.println("Even number " + number);
        }

        // Modify the while code above
        // Make it also record the total number of even numbers it has found
        // and break once 5 are found
        // and at the end, display the total number of even numbers found
        number = 4;
        finishedNumber =  20;
        int evenNumbers = 0;

        while (number <= finishedNumber) {
            number++;
            evenNumbers++;
            if(!isEvenNumber(number)) {
                continue;
            }

            System.out.println("Even number " + number);

            if(evenNumbers == 5) {
                break;
            }
        }

        System.out.println("Total of even numbers: " + evenNumbers);
    }

    // Create a method called isEvenNumber that takes a parameter of type int
    // Its purpose is to determine if the argument passed to the method is
    // an even number or not.
    // return true if an even number, otherwise return false;

    public static boolean isEvenNumber(int number) {
        return number % 2 == 0;
    }
}

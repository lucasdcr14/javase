package br.com.lucas;

public class Main {

    private static final String INVALID_VALUE_MESSAGE = "Invalid value";

    public static void main(String[] args) {
        System.out.println(getDurationString(65, 45));
        System.out.println(getDurationString(3945));
        System.out.println(getDurationString(-41));
        System.out.println(getDurationString(64, 9));
    }

    public static String getDurationString(int minutes, int seconds) {
        if(minutes > 0 && seconds >= 0 && seconds <= 59) {
            int hourResponse = minutes / 60;
            int minutesResponse = minutes % 60;
            minutesResponse += seconds / 60;
            int secondsResponse = seconds % 60;

            String hourString = hourResponse + "h ";
            if(hourResponse < 10) {
                hourString = "0" + hourString;
            }

            String minutesString = minutesResponse + "m ";
            if(minutesResponse < 10) {
                minutesString    = "0" + minutesString;
            }

            String secondsString = secondsResponse + "s ";
            if(secondsResponse < 10) {
                secondsString = "0" + secondsString;
            }

            return hourString + minutesString + secondsString;

        } else {
            return INVALID_VALUE_MESSAGE;
        }
    }

    public static String getDurationString(int seconds) {
        if(seconds >= 0) {
            return getDurationString(seconds / 60, seconds % 60);
        } else {
            return INVALID_VALUE_MESSAGE;
        }
    }
}

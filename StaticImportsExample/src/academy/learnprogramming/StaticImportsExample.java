package academy.learnprogramming;

import static java.lang.Math.*;
import static java.lang.System.out;
import static academy.learnprogramming.Config.*;

public class StaticImportsExample {

    public static void main(String[] args) {
        //int min = Math.min(5, 7);
        int min = min(5, 7);
        out.println("min = " + min);
        out.println(PI);

        printConfig();


        out.println("name = " + NAME);
        out.println("columnColumn" + MAX_COLUMN_COUNT);
    }
}
